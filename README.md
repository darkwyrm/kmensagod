# mensagod: Experimental Mensago server written in Kotlin

## Description

This is a repo containing experimental code for a mensagod rewrite in Kotlin.

I don't want to do this, but I'm also increasingly (1) unhappy with Go as a language and (2) finding more and more bugs and stability problems in the current codebase. A Kotlin port would reduce overhead -- 1 ecosystem of code to maintain -- and potentially make tooling and releases easier overall.

## License

This project is released under the MPL 2.0.

## Project Status

This project is currently experimental and may disappear or be abandoned at any point. Or it might be upgraded to full development status. We'll see. 